![Build status](https://gitlab.cern.ch/Cheburashka/Reksio/badges/master/pipeline.svg)

![Latest release](https://gitlab.cern.ch/Cheburashka/Reksio/-/badges/release.svg?order_by=release_at)

# Reksio - Memory Map Editor

Reksio is a memory map editor for Cheby(YAML) memory maps, used by various tools and generators at CERN.

![Reksio Main Window](./docs/img/reksio_main_window.png)

There are two sub-versions of Reksio:
- internal build with custom non-open source generators (compiled with CMake flag `CERN_BUILD=true`),
- for external users precompiled and uploaded in the [Deploy->Releases](https://gitlab.cern.ch/Cheburashka/reksio/-/releases).

Reksio is based on other open-source Python tools:
 - [PyCheby](https://gitlab.cern.ch/Cheburashka/PyCheby): "cheby" memory map parser,
 - [cheby](https://gitlab.cern.ch/be-cem-edl/common/cheby): HDL and C code generators.

Moreover, CERN's internal build is extended with more code generators available [here](https://gitlab.cern.ch/Cheburashka/code-generators) (to be open-source).

## Table of Content

 - [General Overview](docs/overview.md)
 - [How to build](docs/build.md)
 - [Changelog](docs/changelog.md)

## Contact & Support

CERN SY-RF-CS

reksio-support@cern.ch

cheburashka-support@cern.ch 

## Acknowledgments

- Google Breakpad [v2023.06.01](https://github.com/google/breakpad/tree/v2023.06.01)
- pybind11 [v2.10.0](https://github.com/pybind/pybind11/tree/v2.10.0)