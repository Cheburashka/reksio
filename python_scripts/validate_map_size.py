import reksio

def validate(node):
	return validate_user_size(node)

def getMessage():
	return "User Size cannot be defined for Address-Spaces, please remove the attribute"

def validate_user_size(node):
    """
    When Address-Spaces are used, user size is meaningless, because the Address-Spaces are multidirectional
    """
    root_map = node.parent.parentNode
    user_size = root_map.getAttribute('size').value
    address_spaces = root_map.getChildByType('address-space')

    if address_spaces and user_size:
        return False

    return True
