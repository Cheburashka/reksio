# Reksio - Memory Map Editor
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import reksio
import sys
from pathlib import Path

from expand_submap import get_submap_working_path

def validate(attr):
	main_window = reksio.get_main_window()

	attr_node = attr.parentNode
	node_name = attr_node.name
	parent = attr_node.parent
	
	working_path = get_submap_working_path(attr_node, main_window)
	
	filePath = (working_path / Path(attr.value)).resolve()
	return filePath.exists()

def getMessage():
	return "File does not exist!"
	
def validate_str(value):
	return True
