# Reksio - Memory Map Editor
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import code
import reksio
import sys


class MyInteractiveInterpreter(code.InteractiveConsole):
	def __init__(self, locals=globals(), filename="<console>"):
		super().__init__(locals, filename)
		self.more = 0
		
	def runcode(self, code):
		reksio.updateGlobals(globals())
		return super().runcode(code)
		
	def updatePrompt(self, prompt):
		reksio.updatePrompt(prompt)
		
	def executeCommand(self, command):
		self.more = self.push(command)
		if self.more:
			self.updatePrompt("... ")
		else:
			self.updatePrompt(">>> ")
		
	def finish(self):
		self.resetbuffer()
		self.more = 0
	
	def write(self, data):
		reksio.debug(data)
	
	def updateGlobals(self):
		reksio.updateGlobals(globals())
		
	def getGlobals(self):
		return globals()

def displayhook(value):
	if value is None:
		return
	text = repr(value)
	reksio.debug(text)

# give something print-like
def print_to_debug(*objects, sep=' '):
	reksio.debug(sep.join([str(object) for object in objects]))
	
print = print_to_debug
sys.displayhook = displayhook
