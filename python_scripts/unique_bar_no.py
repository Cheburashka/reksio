# Reksio - Memory Map Editor
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import reksio


def validate(attr):
	attr_node = attr.parentNode
	bar_no = attr.value
	parent = attr_node.parent
	if parent:
		for child in parent.children():
			child_bar_no = child.getAttribute('number')
			if child_bar_no is None:
				continue
			if child_bar_no.value == bar_no and attr_node != child:
				reksio.warn(f"PCI bar number is not unique for {child}!")
				return False
	return True


def getMessage():
	return "PCI bar number is not unique!"
