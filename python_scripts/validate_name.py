import reksio

# C++ keywords validation is only required by C++ generators available only in CERN-specific releases.
# For external users is meaningless.
try:
    from cheburashka.components.base.validators.defs import CPP_KEYWORDS
except ImportError:
    CPP_KEYWORDS = {}

def validate(attr):
    attr_node = attr.parentNode
    node_name = attr_node.name
    parent = attr_node.parent

    if node_name in CPP_KEYWORDS:
        reksio.warn(f"Name '{node_name}' is a C++ keyword. The generated C++ library will not compile.")
        return False

    if parent:
        for child in parent.children():
            if child.name == node_name and attr_node != child:
                reksio.warn(f"Name not unique for {child}!")
                return False

    return True


def getMessage():
    return "Invalid name"
