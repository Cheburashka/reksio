# CI/CD jobs related to creation and upload of ready-to-run Reksio packages

package-linux:
    stage: package
    variables:
        GIT_STRATEGY: none
    tags:
        - cc7
        - acc-deployer
    rules:
        - if: $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/
          when: always
        - when: never # do not run the job in any other case
    script:
        - echo "Creating linux package 'reksio-$CI_COMMIT_TAG-linux-x86_64.tar' in progress..."
        # replace CERN-specific settings file with general for external users
        - cp settings/settings.json gui-build/settings/

        # Add open-source python packages for external users
        - DEST_PCKG_PATH="gui-build/python_scripts/lib/site-packages"
        - mkdir -p $DEST_PCKG_PATH
        - packages=("cheby"  "PyCheby" "yaml" "ruamel")
        - |
            for pckg in "${packages[@]}"; do
              echo "Copying $pckg in progress"
              cp -r "$LINUX_VENV_PATH/lib/python3.11/site-packages/$pckg" $DEST_PCKG_PATH
            done

        # Final Linux package for upload
        - |
            tar -vcf "reksio-$CI_COMMIT_TAG-linux-x86_64.tar" --transform="s/gui-build/reksio-$CI_COMMIT_TAG/g" \
                gui-build/reksio \
                gui-build/python_scripts/*.py \
                gui-build/python_scripts/lib/ \
                gui-build/schema/*.yaml \
                gui-build/settings/*.json

        - echo "Creating linux package 'reksio-$CI_COMMIT_TAG-linux-x86_64.tar' -> done."
    dependencies:
        - build-linux
    artifacts:
        paths:
            - reksio-$CI_COMMIT_TAG-linux-x86_64.tar
        expire_in: 24 hrs

upload-package-linux:
    stage: upload
    variables:
        GIT_STRATEGY: none
    tags:
        - cc7
        - acc-deployer
    rules:
        - if: $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/
          when: always
        - when: never # do not run the job in any other case
    script:
        - echo "Uploading linux package '$CI_PROJECT_NAME-$CI_COMMIT_TAG-linux-x86_64.tar' in progress..."
        - |
            response=$(curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
                            --upload-file $CI_PROJECT_NAME-$CI_COMMIT_TAG-linux-x86_64.tar \
                            $PACKAGE_REGISTRY_URL/$CI_PROJECT_NAME-$CI_COMMIT_TAG-linux-x86_64.tar)

            echo "DEBUG: curl response: ${response}"

            # This extra check is required because curl may fail to upload the file, even if the exit
            # code is 0.
            # Extract message from the response json stored as e.g {"message":"401 Unauthorized"}
            # The expected message for success is "201 Created"
            message=$(echo $response | grep -oP '(?<="message":")[^"]*')
            if [[ "$message" != "201 Created" ]]; then
                echo "curl: uploading file failed: ${response}"
                exit -1
            fi;

        - echo "Uploading linux package '$CI_PROJECT_NAME-$CI_COMMIT_TAG-linux-x86_64.tar' -> done."
    dependencies:
        - package-linux


python-package-windows:
    stage: package
    variables:
        GIT_STRATEGY: none
    tags:
        - w10
        - vs2022
    rules:
        - if: $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/
          when: always
        - when: never # do not run the job in any other case
    script:
        - echo "Creating python windows package 'python311-windows.zip' in progress..."
        - C:\\"Program Files"\\7-Zip\\7z.exe x ".\gui-build\RelWithDebInfo\python311.zip" "-opython311_tmp"
        # Update python package with missing open-source packages for ready-to-run packages for external users
        - set "SRC_PATH=%WINDOWS_VENV_PATH%\Lib\site-packages"
        - set "packages=yaml cheby PyCheby ruamel"
        - echo "SRC_PATH = %SRC_PATH%"
        - echo "packages = %packages%"
        - |
            for %%f in (%packages%) do (
                if exist "%SRC_PATH%\%%f" (
                    echo "Copying %%f package..."
                    xcopy "%SRC_PATH%\%%f" "python311_tmp\%%f" /F /E /I /Y
                ) else (
                    echo "ERROR: Package %%f not found in %SRC_PATH%"
                    EXIT /B -1
                )
            )
        # Create a python zip package for distributable Reksio packages
        - C:\\"Program Files"\\7-Zip\\7z.exe a ".\gui-build\ci\python311-windows.zip" .\python311_tmp\*
        - echo "Creating python windows package 'python311-windows.zip' -> done."
    dependencies:
        - build-windows
    artifacts:
        paths:
            - gui-build\ci\python311-windows.zip
        expire_in: 24 hrs

package-windows:
    stage: package
    variables:
        GIT_STRATEGY: none
    tags:
        - w10
        - vs2022
    rules:
        - if: $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/
          when: always
        - when: never # do not run the job in any other case
    needs:
        - job: python-package-windows
          artifacts: true
        - job: build-windows
          artifacts: true
    script:
        - echo "Creating windows package 'reksio-%CI_COMMIT_TAG%-windows-amd64.zip' in progress..."
        # replace CERN-specific settings file with general for external users
        - xcopy settings\settings.json .\gui-build\RelWithDebInfo\settings\ /y

        # copy python zip file with open source packages for distrubutable Reksio packages for external users
        - xcopy ".\gui-build\ci\python311-windows.zip" ".\gui-build\RelWithDebInfo\python311.zip" /Y /F

        - xcopy gui-build\RelWithDebInfo ".\gui-build\reksio-%CI_COMMIT_TAG%" /F /R /Y /I /E
        - C:\\"Program Files"\\7-Zip\\7z.exe a "reksio-%CI_COMMIT_TAG%-windows-amd64.zip" ".\gui-build\reksio-%CI_COMMIT_TAG%"
        - rmdir /q /s ".\gui-build\reksio-%CI_COMMIT_TAG%"

        - echo "Creating windows package 'reksio-%CI_COMMIT_TAG%-windows-amd64.zip' -> done."
    artifacts:
        paths:
            - reksio-$CI_COMMIT_TAG-windows-amd64.zip
        expire_in: 24 hrs

upload-package-windows:
    stage: upload
    variables:
        GIT_STRATEGY: none
    tags:
        - w10
    rules:
        - if: $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/
          when: always
        - when: never # do not run the job in any other case
    script:
        - echo "Uploading windows package 'reksio-%CI_COMMIT_TAG%-windows-amd64.zip' in progress..."
        - |
            FOR /F "delims=" %%O In ('curl --header "JOB-TOKEN: %CI_JOB_TOKEN%" ^
                                    --upload-file "reksio-%CI_COMMIT_TAG%-windows-amd64.zip" ^
                                    "%PACKAGE_REGISTRY_URL%/reksio-%CI_COMMIT_TAG%-windows-amd64.zip"') ^
            Do Set "RESPONSE=%%O"

            echo "DEBUG: curl response: %RESPONSE%"

            @REM This extra check is required because curl may fail to upload the file, even if the exit
            @REM code is 0.
            @REM Expected response json, if the file is successfully uploaded
            set "EXPECTED={"message":"201 Created"}"
            if Not %RESPONSE% == %EXPECTED% (
                echo "curl: uploading file failed: %RESPONSE%"
                exit -1
            )

        - echo "Uploading windows package 'reksio-%CI_COMMIT_TAG%-windows-amd64.zip' -> done."
    dependencies:
        - package-windows

