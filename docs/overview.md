# Reksio Overview

Reksio consists of several submenus and sub-windows, the most important of which are:

1. Main top menu bar.
2. Node tree view.
3. Attribute view.
4. Child subwindow.
5. Console with debugging/information messages.
6. Python language interactive console.

![Reksio Overview](img/reksio_overview.png)

## Main top menu bar

The main top menu bar is a standard menu familiar from similar GUI application. In addition to the standard functions for file, there is a submenu called "actions" 

![Reksio Console View](img/reksio_actions_view.png)

### Actions

Actions are special operations performed on an opened memory map, such as code generators or map address calculators. Currently Reksio consists of the following code generators:
 - HDL generators based on the "cheby" tool for FPGA developers:
 - "EDGE" Linux driver generators,
 - C++ wrappers generators for the EDGE driver,
 - DSP headers in C for embedded projects (e.g. softcore in FPGA).

> **WARNING**: Some generators listed are only available in CERN internal builds. 

The actions submenu can be changed without recompilation using the JSON setting file in `settings/settings.json`, e.g.:

```
"actions":
[
	{
		"action":
		{
			"name": "Calculate addresses",
			"module": "on_load_node",
			"function": "menu_trigger_addresser_action",
			"hook": "onClick",
			"place": "menuActions",
			"tooltip": "Recalculate the addresses"
		}
	},

	{
		"action":
		{
			"name": "File changed",
			"module": "file_changed",
			"function": "file_changed",
			"hook": "onAttributesFileChanged"
		}
	},
    {...}
]
```

### Help menu

![Reksio ](img/reksio_helpmenu_view.png)

In the help menu, users can find an "About" submenu, which includes info about the Python dependiecies used by Reksio and automatically propagated by CI/CD pipelines and a contact for technical support:

![Reksio ](img/reksio_about_window.png)

## Nodes Tree

Each memory map contains multiple different nodes, which are in a hierarchical relation, and represent the address space in the hardware (e.g. SIS8300KU). Nodes can be added to the map by right-clicking and selecting a new node in the "child" category: 

![Reksio Nodes Tree](img/reksio_nodes_tree_view.png)

Some nodes are not addressable and do not represent address space in memory and have a special purpose (e.g. children of the top map `x-driver-edge`). 

## Attributes view

Each node in the tree has attributes that describe special/basic functionality or some special traits of the node.

![Reksio Attributes View](img/reksio_attributes_tree_view.png)

To add/remove/change an attribute, first select the node and then right-clicking. 

## Console

Reksio provides a standard console with debug and info messages, and also display error messages if any of the generators fail.

Here an example from one of the generators:

![Reksio Console View](img/reksio_console_view.png)


## Python interactive console

Reksio provides an interactive Python console for Python distribution used internally by Reksio under the hood (on Linux) or embedded in ready-to-run packages (on Windows). 

Users can use this interactive console as a standard Python console, but its capabilities may be limited (especially on Windows).

![Reksio Python Console View](img/reksio_python_console_view.png)