# Reksio Changelog

## Reksio v2.1.2 (2024.10.15)

Code-Generators default version set to v3.4.3 containing bug fix for the flag `ignore_version_check` in PyCheby v2.0.2. 

More details in [PyCheby documentation](https://acc-py.web.cern.ch/gitlab/Cheburashka/PyCheby/docs/stable/release_notes/2.0.2.html)
or [issue](https://gitlab.cern.ch/Cheburashka/PyCheby/-/issues/1).

## Reksio v2.1.1 (2024.10.12)

Code-Generators default version set to v3.4.2, updated to work with the "cheby" v1.6.0. Moreover, the 'About' window updated with info for the 'cheby' package.

## Reksio v2.1.0 (2024.10.10)

EDGE4.1 support has been added for the driver and wrappers generators, as implemented in [Code-generators v3.4.0](https://gitlab.cern.ch/Cheburashka/code-generators/-/merge_requests/21) - this is an EXPERIMENTAL and initial version - BARELY TESTED.

## Reksio v2.0.0 (2024.10.10)

Reksio has been improved to work with PyCheby v2.0.0+ (full specs of all changes: [PyCheby!9 (merged)](https://gitlab.cern.ch/Cheburashka/PyCheby/-/merge_requests/9)) and Code-Generators v3.3.0+ ([code-generators!19 (merged)](https://gitlab.cern.ch/Cheburashka/code-generators/-/merge_requests/19)). Moreover, several general improvements have been implemented.
The base address of Address-Space nodes has been hardcoded to 0x0 and no longer depending on the attribute base-addr in the x-driver-edge/pci-bars nodes. Moreover, the attribute x-driver-edge/schema-version has been renamed to x-driver-edge/edge-version and converted into Enum type.

### Changes:

- python_scripts/upgrade_functions.py: added map upgrader for x-driver-edge from v2.0.0 to v3.0.0:
  - the base address of Address-Space nodes hardcoded to 0x0 (broken backward compatibility),
  - removed base address child from x-driver-edge/pci-bars/PCIBar nodes (broken backward compatibility),
  - attribute x-driver-edge/schema-version renamed to x-driver-edge/edge-version and converted into Enum type (broken backward compatibility),
- schema-version/x-driver-edge set to v3.0.0,
- python module updated to work in non GUI mode.
- python_scripts/upgrades.py: memory maps upgrades possible from CLI without running GUI,
- python_scripts: created fileutils.py module (functions moved from before_load.py),
- python_scripts/on_load_node.py: the base address of the Address-Space node hardcoded to 0x0 (PyCheby v2.0.0 +, broken backward compatibility).
- added first unit tests (with maps) for map upgraders from XML and old Cheby formats,
- CI:
  - added test stage for map converters for RF_OPTCOMP and RF_SPS200_LOOPS,
  - ci/deploy.yml: updated rules to default (when: on_success) for all deploy devel jobs,
  - added python-package-versions job for extracting versions of python packages displayed in About window,
- src/mainwindow.cpp: the About window updated with versions of python package dependencies used by generators,
- src/main.cpp: updated description of CLI parser with python package dependencies.

## Reksio v1.5.1 (2024.09.12)

Code-Generators default version set to v3.2.2 in CI/CD. 

## Reksio v1.5.0 (2024.09.03)

Migration to Python3.11 - minimal required changes in CI/CD and python_scripts/tree.py, no functional changes for users.

As of Python3.11, Reksio must work with Code Generators v3.1.0+.

## Reksio v1.4.0 (2024.07.31)

CI/CD package jobs have been improved by moving Reksio files and folders in the ready-to-run packages to a separate sub-directory with the version number.

CI/CD jobs have been upgraded to work with MSVC2022 allowing compilation with C++20, Qt 5.15.2 and Python3.9.13 for Windows users - no functional changes.

Several fixes have been implemented to fix warnings or deprecated statements in breakpad and Reksio source files, such as switch-case fall through, zero-initialized structs, deprecated QString::SkipEmptyParts or unused variables - no functional changes.

### Changes:

- ci/package: Reksio files moved to a sub-directory with the version number ('reksio-X.X.X'),
- .gitlab-ci.yml:  global cache key update with MR ID to avoid cache conflicts in Merge Request pipelines,
- src/mainwindow: removed redundant 'v' prefix from version in the 'About' window.

## Reksio v1.3.3 (2024.07.15)

A pop-up warning has been implemented to prevent users from accidentally loading a custom schema file. Loading a custom schema file can lead to unexpected problems or even crashes, and cannot be silently ignored by users.
Custom schema file can be defined in the user settings file:

```
[schema]                                                                                            
customSchemaPath=../path/to/custom_schema.yaml                                                              
useCustomSchema=true
```

### Changes:

- src/mainwindow.cpp: added a pop-up warning with extra messages, when users try to load a custom schema file defined in user's setting file,
- src/mainwindow.cpp: openFile(): method simplified and refactored - no functional changes,
- src/mainwindow.cpp: added if-def include guard for extra python path (python_scripts/lib/site-packages) for ready-to-run Linux packages,
- CMakeLists.txt: added '-Wextra -g3 -ggdb' flags for Linux builds.

## Reksio v1.3.2 (2024.07.08)

Reksio has been updated to work with Code Generators v3.0.0+ - no functional changes.

## Reksio v1.3.1. (2024.07.08)

Miscellaneous bug fixes for `configure-settings` jobs of CI/CD. Moreover, Code-Generators default version set to v2.2.0. 

## Reksio v1.3.0 (2024.07.05)

Reksio release packages have been updated with open-source python packages (cheby, PyCheby, pyyaml, ruamel) for external users, automatically included by CI/CD jobs. From now on, external users don't need to manually install or prepare separate virtual environments to run Reksio generators for HDL part.

### Changes:

- CI:
  - .gitlab-ci.yml split into two separate scripts (ci/deploy.yml, ci/package.yml),
  - ci/package: added python-package-windows job updating python packages with missing open-source packages,
  - ci/deploy: deploy jobs updated with new distributable python packages for Linux and W10,
- python_scripts/validate_name.py: C++ keywords validation skip for external users (non-CERN releases don't contain C++ generators),
- src/mainwindow.cpp: added extra python path (python_scripts/lib/site-packages) to allow users and CI/CD to include custom packages.


## Reksio v1.2.0 (2024.04.24)

CI/CD support for ready-to-run packages for official releases from the tag (following SEMVER) for Windows and Linux on the gitlab project page under section “Deploy → Releases”.

### Changes:

- `.gitlab-ci.yml`:
  - added package, release and upload stages for official releases from tag,
  - deploy jobs updated to new release directory structure (new folders such as "ci" for test builds, and "releases" for official releases from the tag),
  - deploy, package, release and upload stages restricted to run only for official releases, when the tag follows SEMVER,
  - added deploy-windows-devel job for deploying beta/test builds for Windows users,
  - added cleanup stage,
- CMakeLists.txt: added "REKSIO_VERSION" variable, set to tag value by CI pipelines for official releases,
- src/main.cpp: application version updated to CMake variable "REKSIO_VERSION".

## Reksio v1.1.4 (2024.02.14)

Code-Generators paths have been updated to version 2.1.3 released for python3.9 and including the 'cheby' package (CERN build only).

## Reksio v1.1.3 (2024.02.02)

A quick fix for the incompatibilities between the latest "cheby" version released with python 3.11 and Reksio and code-generators running with python 3.9 - applied only to CERN builds.

## Reksio v1.1.2 (2024.01.18)

C++ keywords Validator has been added to prevent the EDGE Driver or Driver Wrappers from being generated for any node name, that is a C++ keyword, as the Driver Wrapper will never compile, and the EDGE driver may not either. Moreover, unused python scripts have been removed.

### Changes:

- python_scripts:
  - added validate_name.py module for C++ keywords,
  - removed validatePCIBar.py (no longer needed),
  - removed getPCIBarEnum.py (no longer needed),
- map_validator: memory-node validator updated to work with C++ keywords validator,
- schema: removed trailing whitespaces,
- src/main: version updated to v1.1.2.

## Reksio v1.1.1 (2023.12.14)

Bug fix for selecting EDGE Driver version - the style argument in the setting file was ignored while calling the generator from Reksio. Moreover, miscellaneous improvements for error handling & Code Generators version set to v2.0.2 (CI/CD).

### Changes:

- src/memorynode.cpp: added an exception to distinguish between a bad file and a non-existing file,
- CI: Code Generators version set to v2.0.2,
- python_scripts/before_load: loading YAML files updated to work with ruamel.yaml v0.18.5, 
- src/main.cpp: version updated to v1.1.1,
- misc: added docker files for Windows.

## Reksio v1.1.0 (2023.12.11)

Full support for the new Address-Space node has been added (with automatic map to Address-Spaces converter), as discussed: https://gitlab.cern.ch/cohtdrivers/cheby/-/issues/85

Backward compatibility with old formats has been maintained. Address Spaces are valid for VME and PCI-based memory maps.

### Changes:

- General:
  - src/python_mappings: added "fullname" read-only property to Attribute type,
  - src/validator: error logs improved with full node name ("SchemaValidator" class),
  - src/main: Reksio version updated to v1.1.0,
  - src/main: updated Reksio metainfo ("About" pop-up and main window title),
- Schema:
  - reg/x-driver-edge: added register roles attribute for EDGE2&3 drivers, and limited to 3 enumerators {IRQ_V, IRQ_L, ASSERT},
  - reg/x-driver-edge: added Interrupt Controller (INTC) child for EDGE3 driver, and limited to 2 enumarators {INTC_SR, INTC_CR},
  - field/x-driver-edge: added generate attribute to Field node (required by BCCM-VFC and other BI maps),
  - repeat node: added align attribute,
  - block node: added x-driver-edge attribute container,
  - submap/x-driver-edge: added new attributes block-prefix and include,
- Python scripts:
  - added memory map size validator validate_map_size.py,
  - renamed python scripts according to PEP-8 standard (camel-case replaced with snake-case),
  - submap_validator.py renamed to validate_submap.py to keep compatibility with other validators,
  - upgrade_functions.py: bug fix for typo in the function walk_pre_order(),
  - tree.py: added global root property used by memory map to Address-Spaces updater,
  - tools_runner.py: wrappers, EDGE driver, HDL generators updated to work with Address-Spaces,
  - tools_runner.py: default EDGE3 style set for wrappers, EDGE driver, HDL generators,
  - map_validator: added validator for x-driver-edge attribute container (bus_type mandatory),
- Miscellaneous UX improvements:
  - src/customattributesview: automatically remove the entire attribute container if the removed attribute is the last remaining in the container (CustomAttributesView::removeAttribute()).

## Reksio v1.0.0 (2023.10.27)

First official historical release.