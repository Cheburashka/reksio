#include "crashhandler.h"
#include "ui_crashhandler.h"
#include <QDateTime>

CrashHandler::CrashHandler(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CrashHandler)
{
    ui->setupUi(this);

    connect(ui->pushButton, &QPushButton::clicked, this, &CrashHandler::close);

    // fill crash info
    QString text;

    const QStringList& arguments = QCoreApplication::arguments();

    QString dumpFile = arguments.at(1);
    QString appName = arguments.at(2);
    QString appVersion = arguments.at(3);
    QString appPath = arguments.at(4);

    text.append(tr("Application has crashed.\n"
                   "Please create an issue or send a mail to the responsible, describing: \n"
                   "- what you were doing,\n"
                   "- if and how you can reproduce the issue\n"
                   "- the memory map that you were editing\n"
                   "- generated dump file (!) (%1)\n"
                   "- those informations:\n\n").arg(dumpFile));
    text.append("------ CUT FROM HERE ------\n");

    text.append(QString("Application name: %1\n").arg(appName));
    text.append(QString("Version: %1\n").arg(appVersion));
    text.append(QString("Build: %1\n").arg(QString::fromLocal8Bit(TODAY)));
    text.append(QString("Branch/SHA: %1\n").arg(QString::fromLocal8Bit(COMMIT_ID)));
    text.append(QString("Path: %1\n").arg(appPath));
    text.append(QString("Time&date %1\n").arg(QDateTime::currentDateTime().toString("dd/MM/yyyy HH:mm:ss")));

    text.append("------ CUT TO HERE ------\n");
    text.append(QString("Please do not forget to attach the dump file: \n %1 \n").arg(dumpFile));
    ui->textBrowser->setText(text);
}

CrashHandler::~CrashHandler()
{
    delete ui;
}
