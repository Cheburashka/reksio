#ifndef CRASHHANDLER_H
#define CRASHHANDLER_H

#include <QDialog>

namespace Ui {
class CrashHandler;
}

class CrashHandler : public QDialog
{
    Q_OBJECT

public:
    explicit CrashHandler(QWidget *parent = nullptr);
    ~CrashHandler();

private:
    Ui::CrashHandler *ui;
};

#endif // CRASHHANDLER_H
