/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#include "pythonvalidator.h"
#include <pybind11/stl.h>
#include <pybind11/embed.h>
#include <pybind11/pybind11.h>
#include <pybind11/eval.h>

#include "python_mappings.h"
#include "pythonaction.h"
#include "attribute.h"
#include "memorynode.h"


namespace py = pybind11;
using namespace py::literals;

PythonValidator::PythonValidator(const YAML::Node &schema_node, const std::string& node_name): INodeRuleValidator (node_name)
{
    const YAML::Node& pyvalid_node = schema_node["py_valid"];
    if(pyvalid_node && pyvalid_node.IsScalar())
    {
        file_path = pyvalid_node.Scalar();
    }
    else
        throw std::runtime_error("Python validator is wrong!");
}

bool PythonValidator::validate(const std::string &/*value*/) const
{
    return true;
}

bool PythonValidator::validate(const Attribute *attr) const
{
    //return python_action(file_path, std::string("validate"), py::cast(attr)).cast<bool>();
    bool res = false;
    PyStdErrOutStreamRedirect pyOutputRedirect{};
    try
    {
        py::module module = py::module::import(file_path.c_str());
        py::object result = module.attr("validate")(attr);
        res = result.cast<bool>();
    }
    catch(const std::exception& exc)
    {
        qWarning("%s", exc.what());
    }
    return res;
}

bool PythonValidator::validate(const MemoryNode *node) const
{
    bool res = false;
    PyStdErrOutStreamRedirect pyOutputRedirect{};
    try
    {
        py::module module = py::module::import(file_path.c_str());
        py::object result = module.attr("validate")(node);
        res = result.cast<bool>();
    }
    catch(const std::exception& exc)
    {
        qWarning("%s", exc.what());
    }
    return res;
}


std::string PythonValidator::getMessage() const
{
    std::string res;
    PyStdErrOutStreamRedirect pyOutputRedirect{};
    try
    {
        py::module module = py::module::import(file_path.c_str());
        py::object result = module.attr("getMessage")();
        res = result.cast<std::string>();
    }
    catch(const std::exception& exc)
    {
        qWarning("%s", exc.what());
    }
    return res;
}


PyNodeEnumValidator::PyNodeEnumValidator(const YAML::Node &schema_node, const std::string &node_name) :
    INodeRuleValidator (node_name)
{
    if(const YAML::Node& py_enum = schema_node["py_enum"])
    {
        module_name = py_enum.Scalar();
    }
    else
        throw std::runtime_error("PyNodeEnum validator is wrong");
}

bool PyNodeEnumValidator::validate(const std::string &/*value*/) const
{
    return true;
}

bool PyNodeEnumValidator::validate(const Attribute *attr) const
{
    auto enum_values = getEnums(attr);
    return std::find(enum_values.begin(), enum_values.end(), attr->getValue()) != enum_values.end();
}

std::string PyNodeEnumValidator::getMessage() const
{
    return "The value is not correct!";
}

std::vector<std::string> PyNodeEnumValidator::getEnums(const Attribute *attr) const
{
    std::vector<std::string> res;
    PyStdErrOutStreamRedirect pyOutputRedirect{};
    try
    {
        py::module module = py::module::import(module_name.c_str());
        py::object result = module.attr("get_enums")(attr);
        res = result.cast<std::vector<std::string>>();
    }
    catch (const std::exception& exc)
    {
        qWarning("%s", exc.what());
    }
    return res;
}
