/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#include "texteditform.h"
#include "ui_texteditform.h"

TextEditForm::TextEditForm(const QString& text, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextEditForm),
	changed(false)
{
    setWindowFlag(Qt::Window);
    ui->setupUi(this);
    ui->textEdit->setPlainText(text);
    connect(ui->textEdit, &QPlainTextEdit::textChanged, this, &TextEditForm::setChanged);
}

TextEditForm::~TextEditForm()
{
    delete ui;
}

QString TextEditForm::getText() const
{
    return ui->textEdit->toPlainText();
}

QSize TextEditForm::sizeHint() const
{
    return ui->textEdit->sizeHint();
}

bool TextEditForm::wasChanged() const
{
    return changed;
}

void TextEditForm::setChanged()
{
    changed = true;
}
