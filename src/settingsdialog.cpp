/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "mainwindow.h"
#include <QSettings>
#include <QFileDialog>
SettingsDialog::SettingsDialog(MainWindow* main_window) :
    QDialog(main_window),
    ui(new Ui::SettingsDialog),
    main_window(main_window)
{
    ui->setupUi(this);

    setWindowFlag(Qt::Window);
    setAttribute(Qt::WA_DeleteOnClose, false);

    QSettings settings;
    ui->autosaveEnabled->setChecked(settings.value("autosave/enabled", true).toBool());
    ui->autosaveInterval->setValue(settings.value("autosave/interval", 5).toInt());
    ui->autosaveRotation->setValue(settings.value("autosave/rotation", 10).toInt());
    ui->customSchemaEnabled->setChecked(settings.value("schema/useCustomSchema", false).toBool());
    ui->customSchemaPathLineEdit->setText(settings.value("schema/customSchemaPath", "---").toString());
    ui->pythonPath->setText(settings.value("python/customPath", "").toString());
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::on_autosaveEnabled_stateChanged(int arg1)
{
    QSettings settings;
    bool val;
    if(arg1 == Qt::Checked)
    {
        if(!main_window->autosave_timer->isActive())
        {
            main_window->autosave_timer->start();
        }
        val = true;
    }
    else
    {
        main_window->autosave_timer->stop();
        val = false;
    }
    settings.setValue("autosave/enabled", val);
}

void SettingsDialog::on_autosaveInterval_valueChanged(int arg1)
{
    QSettings settings;
    main_window->autosave_timer->setInterval(1000 * 60 * arg1); // in ms
    settings.setValue("autosave/interval", arg1);
}

void SettingsDialog::on_autosaveRotation_valueChanged(int arg1)
{
    QSettings settings;
    main_window->autosave_files_to_keep = arg1;
    settings.setValue("autosave/rotation", arg1);
}

void SettingsDialog::on_customSchemaEnabled_stateChanged(int arg1)
{
    QSettings settings;
    settings.setValue("schema/useCustomSchema", arg1 == Qt::Checked);
}

void SettingsDialog::on_customSchemaPathBrowse_clicked()
{
    const QString file = QFileDialog::getOpenFileName(this, tr("Select schema file"), QDir::currentPath(), "Schema files (*.yaml *.yml)");
    if(!file.isEmpty())
    {
        QSettings settings;
        settings.setValue("schema/customSchemaPath", file);
        ui->customSchemaPathLineEdit->setText(file);
    }
}

void SettingsDialog::on_pythonPath_editingFinished()
{
    const QString& text = ui->pythonPath->text();
    if(!text.isEmpty())
    {
        QSettings settings;
        settings.setValue("python/customPath", text);
    }
}
