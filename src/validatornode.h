/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef VALIDATORNODE_H
#define VALIDATORNODE_H
#include <memory>
#include <vector>
#include "pythonvalidator.h"

struct INodeRuleValidator; // fwd decl
class MemoryNode; // fwd decl
class AttributeContainer; // fwd decl
class AttributeContainerValidator; // fwd decl

class ValidatorNode
{
public:
    explicit ValidatorNode(const std::string& name = "");
    ValidatorNode(const ValidatorNode& rhs) = delete;
    ValidatorNode& operator=(const ValidatorNode& rhs) = delete;
    ValidatorNode& operator=(ValidatorNode&& rhs);
    ValidatorNode(ValidatorNode&& rhs);
    // add validators
    void addChildValidator(std::unique_ptr<ValidatorNode> validator);
    ValidatorNode* addChildValidator(const std::string& name);
    void addPythonValidator(std::unique_ptr<PythonValidator> validator);

    // get child
    ValidatorNode* getChild(const std::string& name) const;

    // is allowed child
    bool isAllowed(const std::string& type) const;
    const std::vector<ValidatorNode*>& getChildren() const;
    std::vector<ValidatorNode*>& getChildren();
    std::vector<std::unique_ptr<ValidatorNode>>& getChildrenMem();
    std::vector<ValidatorNode*> getChildrenWithSpecialSequences() const;
    const AttributeContainerValidator* getAttributes() const;
    AttributeContainerValidator* getAttributes();

    bool validate(const MemoryNode* node, const bool recursive = true);
    bool isValid(const MemoryNode* node, const bool recursive = true) const;
    const std::string& getName() const;
    std::vector<std::string> getRelativeName(const ValidatorNode* parent) const;
    ValidatorNode* getParent() const;
    void setParent(ValidatorNode* parent);
    bool isAttributeSequence() const;
    void setAttributeSequence(bool val);
    bool isChildrenSequence() const;
    void setChildrenSequence(bool val);

    bool isSpecialSequence() const;

    // python validator
    bool runPythonValidator(const MemoryNode* node);
    bool hasPythonValidator() const;
    PythonValidator* getPythonValidator() const;

    bool isDeprecated() const;
    void setDeprecated(bool deprecated);
    const std::string& getDeprecatedMessage() const;
    void setDeprecatedMessage(const std::string& msg);
    // python menu actions
    void addPythonMenuAction(const std::string& name, const std::string& py_module, const std::string& py_function);
    const std::vector<std::pair<std::string, std::pair<std::string, std::string>>>& getPythonMenuActions() const;

protected:
    void updateParent();
    std::unique_ptr<AttributeContainerValidator> _attributes;
    std::vector<ValidatorNode*> _children;
    std::vector<std::unique_ptr<ValidatorNode>> _children_mem;
    std::string _name;
    ValidatorNode* _parent = nullptr;
    bool _attribute_sequence;
    bool _children_sequence;
    std::unique_ptr<PythonValidator> _py_validator;
    bool _deprecated = false; // if item is deprecated
    std::string _deprecated_msg; // reason of deprecation
    std::vector<std::pair<std::string, std::pair<std::string, std::string>>> _py_menu_actions;
};

#endif // VALIDATORNODE_H
