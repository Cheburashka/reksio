/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef PYTHONINTERPRETERLINEEDIT_H
#define PYTHONINTERPRETERLINEEDIT_H

#include <QLineEdit>
#include <QStringList>
#include <QDialog>
#include <pybind11/pybind11.h>

class PythonInterpreterLineEdit : public QLineEdit
{
public:
    explicit PythonInterpreterLineEdit(QWidget* parent = nullptr);
    void keyPressEvent(QKeyEvent* event) override;
    bool event(QEvent* event) override;
private:
    int _history_index = 0;
    QStringList _history;
    QStringList getCompleterList(const QString& text);
    pybind11::dict _globals;
    pybind11::module _rlcompleter;
    friend class PythonInterpreterWidget;
public Q_SLOTS:
    void finished(int);
    void updateCompleter(const QString& text);
};

class PythonInterpreterWidget : public QWidget
{
public:
    explicit PythonInterpreterWidget(QWidget* parent = nullptr);
    void updatePrompt(const QString& prompt);
    void updateGlobals(pybind11::dict globals);
public Q_SLOTS:
    void sendCommand();

protected:
    PythonInterpreterLineEdit* line_edit;
    pybind11::object interpreter;
};

#endif // PYTHONINTERPRETERLINEEDIT_H
