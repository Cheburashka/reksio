/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef SEARCHWINDOW_H
#define SEARCHWINDOW_H

#include <QWidget>

class MainWindow;

class QTableWidgetItem;
class MemoryNode;
class Attribute;

namespace Ui {
class SearchWindow;
}

class SearchWindow : public QWidget
{
    Q_OBJECT

public:
    enum DataTypes
    {
        MemoryNodePtr = Qt::UserRole,
        AttributePtr
    };
    explicit SearchWindow(MainWindow* main_window);
    ~SearchWindow();

private Q_SLOTS:
    void on_searchButton_clicked();

    void on_searchField_returnPressed();

    void on_resultsTableWidget_itemClicked(QTableWidgetItem *item);

    void on_regularExpressionCheckBox_stateChanged(int arg1);

private:
    void setData(QTableWidgetItem *item, MemoryNode* node, Attribute* attr);
    Ui::SearchWindow *ui;
    MainWindow* main_window;
    bool using_regex;
};

#endif // SEARCHWINDOW_H
