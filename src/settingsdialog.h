/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

class MainWindow;


namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(MainWindow* main_window);
    ~SettingsDialog();

private Q_SLOTS:
    void on_autosaveEnabled_stateChanged(int arg1);

    void on_autosaveInterval_valueChanged(int arg1);

    void on_autosaveRotation_valueChanged(int arg1);

    void on_customSchemaEnabled_stateChanged(int arg1);

    void on_customSchemaPathBrowse_clicked();

    void on_pythonPath_editingFinished();

private:
    Ui::SettingsDialog *ui;
    MainWindow* main_window;
};

#endif // SETTINGSDIALOG_H
