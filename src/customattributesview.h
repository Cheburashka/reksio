/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef CUSTOMATTRIBUTESVIEW_H
#define CUSTOMATTRIBUTESVIEW_H

#include <QTreeView>
#include <QMenu>
#include <QUndoStack>

class AttributesModel;
class CustomNodesView;

class CustomAttributesView : public QTreeView
{
    Q_OBJECT
public:
    explicit CustomAttributesView(CustomNodesView* nodesView, QWidget *parent = nullptr);
    void keyPressEvent(QKeyEvent* event) override;
    AttributesModel* getAttributesModel();
    void setUndoStack(QUndoStack* stack);

public Q_SLOTS:
    void onCustomContextMenu(const QPoint & point);
    void removeAttribute();

private:
    CustomNodesView* nodesView;
    QUndoStack* undoStack;
    QMenu* attributesContextMenu;
    QMenu* add_attribute_submenu;
    QAction * remove_attribute;
    QAction * remove_attribute_container;
};

#endif // CUSTOMATTRIBUTESVIEW_H
