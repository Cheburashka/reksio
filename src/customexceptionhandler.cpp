/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#include "customexceptionhandler.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QProcess>

#if defined(Q_OS_LINUX)
#  include "client/linux/handler/exception_handler.h"
#elif defined(Q_OS_WIN)
#  include "client/windows/handler/exception_handler.h"
#endif

QString CustomExceptionHandler::applicationPath;
QString CustomExceptionHandler::crashHandlerName;

#if defined(Q_OS_LINUX)
static bool exceptionHandlerCallback(const google_breakpad::MinidumpDescriptor& descriptor,
                                     void* /*context*/,
                                     bool succeeded)
{
    if (!succeeded)
        return succeeded;

    const QStringList argumentList = {
        QString::fromLocal8Bit(descriptor.path()),
        QCoreApplication::applicationName(),
        QCoreApplication::applicationVersion(),
        QCoreApplication::applicationFilePath()
    };
    const QString crash_handler = CustomExceptionHandler::applicationPath + "/" + CustomExceptionHandler::crashHandlerName;

    return !QProcess::execute(crash_handler, argumentList);
}
#elif defined(Q_OS_WIN)
static bool exceptionHandlerCallback(const wchar_t* dump_path,
                                     const wchar_t* minidump_id,
                                     void* context,
                                     EXCEPTION_POINTERS* exinfo,
                                     MDRawAssertionInfo* assertion,
                                     bool succeeded)
{
    Q_UNUSED(assertion);
    Q_UNUSED(exinfo);
    Q_UNUSED(context);

    if (!succeeded)
        return succeeded;

    const QString path = QString::fromWCharArray(dump_path, int(wcslen(dump_path))) + '/'
            + QString::fromWCharArray(minidump_id, int(wcslen(minidump_id))) + ".dmp";

    const QStringList argumentList = {
        path,
        QCoreApplication::applicationName(),
        QCoreApplication::applicationVersion(),
        QCoreApplication::applicationFilePath()
    };
    const QString crash_handler = CustomExceptionHandler::applicationPath + "/" + CustomExceptionHandler::crashHandlerName;
    return !QProcess::execute(crash_handler, argumentList);
}
#endif


#if defined(Q_OS_LINUX)
CustomExceptionHandler::CustomExceptionHandler(const QString& appPath):
    exception_handler_(new google_breakpad::ExceptionHandler(
                           google_breakpad::MinidumpDescriptor(QDir::tempPath().toStdString()),
                          nullptr,
                          exceptionHandlerCallback,
                          nullptr,
                          true,
                          -1))
{
    applicationPath = appPath;
    crashHandlerName = "bin_crash_handler";
}
#elif defined(Q_OS_WIN)
CustomExceptionHandler::CustomExceptionHandler(const QString& appPath)
    : exception_handler_(new google_breakpad::ExceptionHandler(
                           QDir::tempPath().toStdWString(),
                           nullptr,
                           exceptionHandlerCallback,
                           nullptr,
                           google_breakpad::ExceptionHandler::HANDLER_ALL))
{
    applicationPath = appPath;
    crashHandlerName = "bin_crash_handler.exe";
}

#endif

void CustomExceptionHandler::crash()
{
    volatile int *a = (int*)0x666;
    int b = *a;
    qWarning() << b;
}

CustomExceptionHandler::~CustomExceptionHandler()
{
    if(exception_handler_)
        delete exception_handler_;
}
