/*
 * Reksio - Memory Map Editor
 * Copyright (C) 2023 CERN
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization or
 * submit itself to any jurisdiction.
 */

#ifndef PYTHONACTION_H
#define PYTHONACTION_H
#include <QString>
#include <QMetaType>
#include "python_mappings.h"


class PythonAction
{
public:
    explicit PythonAction(const QString& name, const QString& module, const QString& function);
    explicit PythonAction() = default;
    QString name;
    QString module;
    QString function;
};

Q_DECLARE_METATYPE(PythonAction)

template<typename ... Arguments>
py::object python_action(const std::string& script_name, const std::string& function_name, Arguments ... args)
{
    PyStdErrOutStreamRedirect pyOutputRedirect{};
    py::object result = py::cast(false);
    try
    {
        py::module module = py::module::import(script_name.c_str());
        result = module.attr(function_name.c_str())(args...);
    }
    catch (const std::exception& exc)
    {
        qWarning("%s", exc.what());
    }
    return result;
}

#endif // PYTHONACTION_H
