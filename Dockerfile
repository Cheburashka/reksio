# This Dockerfile provides a build environment for Windows (MS Visual Studio 2017+Python 3+Qt).
# It needs to be started with at least 2 GB of RAM.
# docker run --memory 2048mb --rm -it gitlab-registry.cern.ch/cheburashka/gui:windows

FROM chocolatey/choco:latest-windows

RUN choco install --yes python39

# Install Qt
RUN C:\Python39\python.exe -m pip install aqtinstall
RUN C:\Python39\python.exe -m aqt install-qt windows desktop 5.12.9 win64_msvc2017_64
RUN C:\Python39\python.exe -m pip uninstall --yes aqtinstall

# Install MS Visual Studio Community 2017
RUN choco install --yes --package-parameters "--add Microsoft.VisualStudio.Component.VC.CMake.Project --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --add Microsoft.VisualStudio.Component.Windows10SDK.17763" visualstudio2017community

COPY env.bat C:\\
CMD cmd.exe /K env.bat
