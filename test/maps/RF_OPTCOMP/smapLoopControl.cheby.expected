memory-map:
  name: smapLoopControl
  bus: cern-be-vme-16
  size: 256
  x-enums:
    - enum:
        name: stepCtrl_mux
        width: 3
        children:
          - item:
              name: selDly0
              description: 90-deg Phase rotation
              value: 0
          - item:
              name: selDly1
              description: Forward path control
              value: 1
          - item:
              name: selDly2
              description: Return path control
              value: 2
          - item:
              name: selDly3
              description: Pulse feedback
              value: 3
          - item:
              name: selDly4
              description: Fc signal
              value: 4
          - item:
              name: selDly5
              description: Frep signal
              value: 5
    - enum:
        name: xbCtrl_mux
        width: 2
        children:
          - item:
              name: selXB0
              description: Select crossbar 0
              comment: Forward path crossbar
              value: 0
          - item:
              name: selXB1
              description: Select crossbar 1
              comment: Return path crossbar
              value: 1
          - item:
              name: selXB2
              description: Select crossbar 2
              comment: Phase detector return path crossbar
              value: 2
    - enum:
        name: xbCtrl_value
        width: 2
        children:
          - item:
              name: out0in0out1in0
              description: Both inputs to Out 0
              value: 0
          - item:
              name: out0in1out1in0
              description: In 0 to Out 1; In 1 to Out 0
              value: 1
          - item:
              name: out0in0out1in1
              description: In 0 to Out 0; In 1 to Out 1
              value: 2
          - item:
              name: out0in1out1in1
              description: Both inputs to Out 1
              value: 3
    - enum:
        name: sfpCtrl_mux
        width: 1
        children:
          - item:
              name: sel0
              description: Select SFP0 (local)
              value: 0
          - item:
              name: sel1
              description: Select SFP1 (remote)
              value: 1
    - enum:
        name: ledCtrl_mux
        width: 3
        children:
          - item:
              name: sel0
              description: Local LED
              value: 0
          - item:
              name: sel1
              description: Remote LED
              value: 1
          - item:
              name: sel2
              description: Locked LED
              value: 2
          - item:
              name: sel3
              description: Step LED
              value: 3
          - item:
              name: sel4
              description: Status LED
              value: 4
    - enum:
        name: ledCtrl_value
        width: 3
        children:
          - item:
              name: pink
              description: All ON
              value: 0
          - item:
              name: lightBlue
              description: Blue and Green ON
              value: 1
          - item:
              name: violet
              description: Blue and Red ON
              value: 2
          - item:
              name: blue
              description: Blue ON
              value: 3
          - item:
              name: orange
              description: Green and Red ON
              value: 4
          - item:
              name: green
              description: Green ON
              value: 5
          - item:
              name: red
              description: Red ON
              value: 6
          - item:
              name: 'off'
              description: All OFF
              value: 7
    - enum:
        name: control2_rModeIn
        width: 2
        children:
          - item:
              name: cal90deg
              value: 0
          - item:
              name: calLocal1
              value: 1
          - item:
              name: calLocal2
              value: 2
          - item:
              name: operation
              value: 3
    - enum:
        name: crossbars_xbar0
        width: 2
        children:
          - item:
              name: out0in0out1in0
              description: Both inputs to Out 0
              value: 0
          - item:
              name: out0in1out1in0
              description: In 0 to Out 1; In 1 to Out 0
              value: 1
          - item:
              name: out0in0out1in1
              description: In 0 to Out 0; In 1 to Out 1
              value: 2
          - item:
              name: out0in1out1in1
              description: Both inputs to Out 1
              value: 3
    - enum:
        name: crossbars_xbar1
        width: 2
        children:
          - item:
              name: out0in0out1in0
              description: Both inputs to Out 0
              value: 0
          - item:
              name: out0in1out1in0
              description: In 0 to Out 1; In 1 to Out 0
              value: 1
          - item:
              name: out0in0out1in1
              description: In 0 to Out 0; In 1 to Out 1
              value: 2
          - item:
              name: out0in1out1in1
              description: Both inputs to Out 1
              value: 3
    - enum:
        name: crossbars_xbar2
        width: 2
        children:
          - item:
              name: out0in0out1in0
              description: Both inputs to Out 0
              value: 0
          - item:
              name: out0in1out1in0
              description: In 0 to Out 1; In 1 to Out 0
              value: 1
          - item:
              name: out0in0out1in1
              description: In 0 to Out 0; In 1 to Out 1
              value: 2
          - item:
              name: out0in1out1in1
              description: Both inputs to Out 1
              value: 3
  x-gena:
    map-version: 20160901
  x-map-info:
    ident: 0x4e
  schema-version:
    core: 3.0.0
    x-conversions: 1.0.0
    x-driver-edge: 3.0.0
    x-enums: 1.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
  children:
    - reg:
        name: control1
        description: Control bits of manual mode
        width: 16
        access: rw
        address: next
        x-gena:
          rmw: true
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: manualMode
              description: Manual mode multiplexers selector
              range: 0
              preset: 0x0
          - field:
              name: fineDlyValid0
              description: Change FineDly0 with fineDly0 (valid only when manual mode is active)
              range: 1
              x-gena:
                auto-clear: 1
          - field:
              name: fineDlyValid1
              description: Change FineDly1 with fineDly1 (valid only when manual mode is active)
              range: 2
              x-gena:
                auto-clear: 1
          - field:
              name: fineDlyValid2
              description: Change FineDly2 with fineDly2 (valid only when manual mode is active)
              range: 3
              x-gena:
                auto-clear: 1
          - field:
              name: stepChange
              description: Change step with stepCtrl data (valid only when manual mode is active)
              range: 4
              x-gena:
                auto-clear: 1
          - field:
              name: xbChange
              description: Change crossbar with xbCtrl data (valid only when manual mode is active)
              range: 5
              x-gena:
                auto-clear: 1
          - field:
              name: sfpChange
              description: Change SFP with sfpCtrl data (valid only when manual mode is active)
              range: 6
              x-gena:
                auto-clear: 1
          - field:
              name: ledChange
              description: Change LED with ledCtrl data (valid only when manual mode is active)
              range: 7
              x-gena:
                auto-clear: 1
    - reg:
        name: status1
        description: Status bits of manual mode and controller
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: stepBusy
              description: Busy signal in interface to step control
              range: 0
          - field:
              name: xbBusy
              description: Busy signal in interface to crossbar control
              range: 1
          - field:
              name: sfpBusy
              description: Busy signal in interface to SFP control
              range: 2
          - field:
              name: ledBusy
              description: Busy signal in interface to LED control
              range: 3
          - field:
              name: coarseLocked
              description: Coarse Loop locked
              range: 4
          - field:
              name: coarseEnabled
              description: Coarse Loop enabled
              range: 5
          - field:
              name: fineEnabled
              description: Fine Loop enabled
              range: 6
          - field:
              name: allowStepping
              description: Step Up/Down allowed
              range: 7
          - field:
              name: stepping
              description: Currently stepping flag
              range: 8
          - field:
              name: calDone
              description: Calibration done
              range: 9
          - field:
              name: locked
              description: Both loops locked
              range: 10
          - field:
              name: tuningDel0
              description: Tuning delay line 0
              range: 11
          - field:
              name: tuningDel1
              description: Tuning delay line 1
              range: 12
          - field:
              name: tuningDel2
              description: Tuning delay line 2
              range: 13
          - field:
              name: warningStepUp
              description: Warning to Step Up
              range: 14
          - field:
              name: warningStepDown
              description: Warning to Step Down
              range: 15
    - reg:
        name: filterLength
        description: Length of dynamic phase filter
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: samples
              description: Averaging time of phase filter
              range: 3-0
              preset: 0xa
              x-fesa:
                unit: s
              x-conversions:
                read: pow(2,val)/(samplingFreq*1000.0)
                write: log(val*samplingFreq*1000.0)/log(2)
    - reg:
        name: initStep0
        description: Value of Initial Step 0
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              range: 9-0
              preset: 0x1f4
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
                write: val/10
          - field:
              name: loadValue
              description: Load InitStep
              range: 15
              preset: 0x0
              x-gena:
                auto-clear: 1
    - reg:
        name: initStep1
        description: Value of Initial Step 1
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              range: 9-0
              preset: 0x1f4
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
                write: val/10
          - field:
              name: loadValue
              range: 15
              preset: 0x0
              x-gena:
                auto-clear: 1
    - reg:
        name: initStep2
        description: Value of Initial Step 2
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              range: 9-0
              preset: 0x1f4
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
                write: val/10
          - field:
              name: loadValue
              range: 15
              preset: 0x0
              x-gena:
                auto-clear: 1
    - reg:
        name: initStep3
        description: Value of Initial Step 3
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              range: 9-0
              preset: 0x1f4
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
                write: val/10
          - field:
              name: loadValue
              range: 15
              preset: 0x0
              x-gena:
                auto-clear: 1
    - reg:
        name: initStep4
        description: Value of Initial Step 4
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              range: 9-0
              preset: 0x1f4
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
                write: val/10
          - field:
              name: loadValue
              range: 15
              preset: 0x0
              x-gena:
                auto-clear: 1
    - reg:
        name: initStep5
        description: Value of Initial Step 5
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              range: 9-0
              preset: 0x1f4
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
                write: val/10
          - field:
              name: loadValue
              description: Load InitStep
              range: 15
              preset: 0x0
              x-gena:
                auto-clear: 1
    - reg:
        name: fineDly0
        description: Manual mode value of Fine Delay 0 (90-deg phase rotation)
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: value
              description: Fine Tune voltage of Delay Line 0
              range: 17-0
              x-fesa:
                unit: V
              x-conversions:
                read: (val+pow(2,17))*2500.0/(1510*pow(2,18))
                write: (val*(1510*pow(2,18))/2500.0)-pow(2,17)
    - reg:
        name: fineDly1
        description: Manual mode value of Fine Delay 1 (Forward path control)
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: value
              description: Fine Tune voltage of Delay Line 1
              range: 17-0
              x-fesa:
                unit: V
              x-conversions:
                read: (val+pow(2,17))*2500.0/(1510*pow(2,18))
                write: (val*(1510*pow(2,18))/2500.0)-pow(2,17)
    - reg:
        name: fineDly2
        description: Manual mode value of Fine Delay 2 (Return path control)
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: value
              description: Fine Tune voltage of Delay Line 2
              range: 17-0
              x-fesa:
                unit: V
              x-conversions:
                read: (val+pow(2,17))*2500.0/(1510*pow(2,18))
                write: (val*(1510*pow(2,18))/2500.0)-pow(2,17)
    - reg:
        name: stepCtrl
        description: Step Values control
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: mux
              description: Delay line selector
              range: 2-0
              x-enums:
                name: stepCtrl_mux
          - field:
              name: value
              description: Delay value
              range: 13-4
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
                write: val/10
          - field:
              name: latch
              description: Latch Enable (LEN signal of Delay line)
              range: 14
    - reg:
        name: xbCtrl
        description: Crossbars signals control
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: mux
              description: Crossbar selector
              range: 1-0
              x-enums:
                name: xbCtrl_mux
          - field:
              name: value
              description: Crossbar configuration
              range: 5-4
              x-enums:
                name: xbCtrl_value
    - reg:
        name: sfpCtrl
        description: SFP signals control
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: mux
              description: SFP selector
              range: 0
              x-enums:
                name: sfpCtrl_mux
          - field:
              name: rate
              description: Rate selection
              comment: Low selects reduced bandwitdth
              range: 1
          - field:
              name: txDis
              description: TX disable
              comment: Optical output disabled when high
              range: 2
    - reg:
        name: ledCtrl
        description: LED signals control
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: false
        children:
          - field:
              name: mux
              description: LED selector
              range: 2-0
              x-enums:
                name: ledCtrl_mux
          - field:
              name: value
              description: LED color
              range: 6-4
              x-enums:
                name: ledCtrl_value
    - reg:
        name: control2
        description: Control bits of phase statistics and some loop features
        width: 16
        access: rw
        address: next
        x-gena:
          rmw: true
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: phaseDiffRst
              description: Reset PhaseDiff statistics
              range: 0
              x-gena:
                auto-clear: 1
          - field:
              name: phaseFiltDiffRst
              description: Reset PhaseFiltDiff statistics
              range: 1
              x-gena:
                auto-clear: 1
          - field:
              name: phaseFiltSelector
              description: Select input to Fine and Coarse Loops
              range: 2
              preset: 0x1
          - field:
              name: stepUp
              description: Manually StepUp (when Fine loop is enabled)
              range: 3
              x-gena:
                auto-clear: 1
          - field:
              name: stepDown
              description: Manually StepDown (when Fine loop is enabled)
              range: 4
              x-gena:
                auto-clear: 1
          - field:
              name: forceAllowStepping
              description: Manually force AllowStepping independently of running mode
              range: 5
              preset: 0x0
          - field:
              name: rModeIn
              description: Manually select the running mode
              range: 7-6
              x-enums:
                name: control2_rModeIn
    - reg:
        name: samplingFreq
        description: ADC sampling frequency
        width: 16
        access: rw
        address: next
        preset: 0x0
        x-fesa:
          multiplexed: false
          persistence: true
          unit: kHz
        x-conversions:
          read: 50000.0/((val*430.0/pow(2,16))+70.0)
          write: (pow(2,16)/430.0)*((50000.0/val)-70.0)
    - reg:
        name: phaseRaw
        description: Raw value of phase difference
        comment: It is computed from the difference between the digitized value of Ch1 and the one from Ch2 of the ADC.
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
    - reg:
        name: phaseFilt
        description: Filtered value of phase difference
        comment: It is computed from the difference between the digitized value of Ch1 and the one from Ch2 of the ADC.
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
    - reg:
        name: phaseRawDiffMin
        description: Minimum value of difference in raw phase
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
    - reg:
        name: phaseRawDiffMax
        description: Maximum value of difference in raw phase
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
    - reg:
        name: phaseFiltDiffMin
        description: Minimum value of difference in filtered phase
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
    - reg:
        name: phaseFiltDiffMax
        description: Maximum value of difference in filtered phase
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
    - reg:
        name: setPoint
        description: Set point value for loops
        width: 16
        access: rw
        address: next
        preset: 0x0
        x-fesa:
          multiplexed: false
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
          write: (val*pow(2,16))/180.0
    - reg:
        name: coarseLoopFactor
        description: Loop factor for Coarse Loop
        width: 16
        access: rw
        address: next
        preset: 0x4000
        x-fesa:
          multiplexed: false
          persistence: true
    - reg:
        name: coarseLoopPeriod
        description: Loop period for Coarse Loop
        width: 16
        access: rw
        address: next
        preset: 0x71c7
        x-fesa:
          multiplexed: false
          persistence: true
          unit: ms
        x-conversions:
          read: (val*(450000.0/pow(2,16))+50000.0)/50000.0
          write: (pow(2,16)/450000.0)*(50000.0*val-50000.0)
    - reg:
        name: fineLoopFactor
        description: Loop factor for Fine Loop
        width: 16
        access: rw
        address: next
        preset: 0xff9c
        x-fesa:
          multiplexed: false
          persistence: true
    - reg:
        name: fineLoopPeriod
        description: Loop period for Fine Loop
        width: 16
        access: rw
        address: next
        preset: 0x1746
        x-fesa:
          multiplexed: false
          persistence: true
          unit: us
        x-conversions:
          read: ((val*49500.0/pow(2,16))+500.0)/50.0
          write: (pow(2,16)/49500.0)*(50.0*val-500.0)
    - reg:
        name: fineLoopInitial
        description: Initial Value for Fine Loop
        width: 16
        access: rw
        address: next
        preset: 0xcd50
        x-fesa:
          multiplexed: false
          persistence: true
          unit: V
        x-conversions:
          read: (val*4+pow(2,17))*2500.0/(1510.0*pow(2,18))
          write: ((val*(1510*pow(2,18))/2500.0)-pow(2,17))/4.0
    - reg:
        name: fineLoopHighLimit
        description: Fine Loop High Limit
        width: 16
        access: rw
        address: next
        preset: 0x1a9f
        x-fesa:
          multiplexed: false
          persistence: true
          unit: V
        x-conversions:
          read: (val*4+pow(2,17))*2500.0/(1510.0*pow(2,18))
          write: ((val*(1510*pow(2,18))/2500.0)-pow(2,17))/4.0
    - reg:
        name: fineLoopLowLimit
        description: Fine Loop Low Limit
        width: 16
        access: rw
        address: next
        preset: 0x9688
        x-fesa:
          multiplexed: false
          persistence: true
          unit: V
        x-conversions:
          read: (val*4+pow(2,17))*2500.0/(1510.0*pow(2,18))
          write: ((val*(1510*pow(2,18))/2500.0)-pow(2,17))/4.0
    - reg:
        name: control3
        description: Extra control bits of loops and dac errors
        width: 16
        access: rw
        address: next
        x-gena:
          rmw: true
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: keepFineDelay
              description: Avoid setting initial value of Fine Delay when coarse loop is enabled
              range: 0
              preset: 0x0
          - field:
              name: skipCoarseLoop
              description: Skip coarse loop and go directly to fine loop
              range: 1
              preset: 0x0
          - field:
              name: resetDacError0
              description: Reset DAC error statistics
              range: 2
              x-gena:
                auto-clear: 1
          - field:
              name: resetDacError1
              description: Reset DAC error statistics
              range: 3
              x-gena:
                auto-clear: 1
          - field:
              name: resetDacError2
              description: Reset DAC error statistics
              range: 4
              x-gena:
                auto-clear: 1
    - reg:
        name: coarseLockedThresh
        description: Coarse loop locked threshold common to all coarse loops
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              description: Difference between the two last values
              comment: Difference between the two most probable values must be higher than value to be locked
              range: 7-0
              preset: 0xa
    - reg:
        name: fineLockedThresh
        description: Fine loop locked threshold common to all fine loops
        width: 16
        access: rw
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: value
              description: Difference between the two last values (6 MSBs)
              comment: Difference between the two most probable values (6 MSBs) must be higher than value to be locked
              range: 7-0
              preset: 0x32
    - reg:
        name: phaseError
        description: Phase error at the input of coarse and fine loops
        comment: Difference between phase value (raw or filtered) and set point at the input of the loops. It is the same as phaseRaw or phaseFilt, depending on the value of phaseFiltSelector.
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
          unit: deg RF
        x-conversions:
          read: val*180.0/(pow(2,16))
    - reg:
        name: coarseDel0
        description: Current Step value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: value
              description: Current Step value
              range: 9-0
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
    - reg:
        name: coarseDel1
        description: Current Step value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: value
              description: Current Step value
              range: 9-0
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
    - reg:
        name: coarseDel2
        description: Current Step value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: value
              description: Current Step value
              range: 9-0
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
    - reg:
        name: coarseDel3
        description: Current Step value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: value
              description: Current Step value
              range: 9-0
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
    - reg:
        name: coarseDel4
        description: Current Step value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: value
              description: Current Step value
              range: 9-0
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
    - reg:
        name: coarseDel5
        description: Current Step value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: value
              description: Current Step value
              range: 9-0
              x-fesa:
                unit: ps
              x-conversions:
                read: val*10
    - reg:
        name: fineDel0
        description: Current fine delay value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
          unit: V
        x-conversions:
          read: (val+pow(2,15))*2500.0/(1510*pow(2,16))
    - reg:
        name: fineDel1
        description: Current fine delay value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
          unit: V
        x-conversions:
          read: (val+pow(2,15))*2500.0/(1510*pow(2,16))
    - reg:
        name: fineDel2
        description: Current fine delay value
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
          unit: V
        x-conversions:
          read: (val+pow(2,15))*2500.0/(1510*pow(2,16))
    - reg:
        name: crossbars
        description: Current position of crossbars
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: true
          persistence: true
        children:
          - field:
              name: xbar0
              description: Forward path crossbar
              range: 1-0
              x-enums:
                name: crossbars_xbar0
          - field:
              name: xbar1
              description: Return path crossbar
              range: 5-4
              x-enums:
                name: crossbars_xbar1
          - field:
              name: xbar2
              description: Phase detector return path crossbar
              range: 9-8
              x-enums:
                name: crossbars_xbar2
    - reg:
        name: dacError0
        description: DAC Error counter
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
    - reg:
        name: dacError1
        description: DAC Error counter
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
    - reg:
        name: dacError2
        description: DAC Error counter
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
