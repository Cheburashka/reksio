memory-map:
  bus: wb-32-be
  name: dac
  description: DAC Control
  word-endian: little
  x-wbgen:
    hdl_entity: dac_ctrl_wb
  schema-version:
    core: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-fesa: 2.0.0
    x-driver-edge: 1.0.0
    x-conversions: 1.0.0
    x-wbgen: 1.0.0
    x-map-info: 1.0.0
    x-enums: 1.0.0
  x-hdl:
    busgroup: true
    reg-prefix: true
  children:
    - reg:
        name: odelay
        address: 0x00000000
        width: 32
        access: rw
        description: Output Delay Control
        children:
          - field:
              name: rst_idelayctrl
              range: 0
              description: Reset Output IDELAYCTRL
              x-wbgen:
                type: BIT
                clock: clk_dac_i
          - field:
              name: rst_odelay
              range: 1
              description: Reset Output ODELAY
              x-wbgen:
                type: BIT
                clock: clk_dac_i
          - field:
              name: rst_oserdes
              range: 2
              description: Reset Output OSERDES
              x-wbgen:
                type: BIT
                clock: clk_dac_i
          - field:
              name: rdy
              range: 3
              description: Output Delay Ready
              x-wbgen:
                type: BIT
                access_bus: READ_ONLY
                access_dev: WRITE_ONLY
                clock: clk_dac_i
          - field:
              name: value
              range: 12-4
              description: Output Delay Value
              comment: "Delay value in taps\n"
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
                clock: clk_dac_i
          - field:
              name: value_update
              range: 13
              description: Delay value update
              x-wbgen:
                type: MONOSTABLE
                clock: clk_dac_i
          - field:
              name: bitslip
              range: 15-14
              description: Serdes Bitslip
              comment: "Bit slip of the DAC output serdes. Together with the value field, it allows output delay adjustment range of a full DAC clock cycle. Expressed in nanoseconds (0 to 4).\n"
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
                clock: clk_dac_i
    - reg:
        name: odelay_calib
        address: 0x00000004
        width: 32
        access: rw
        description: Output Delay Calibration
        children:
          - field:
              name: en_vtc
              range: 0
              description: Enable VT compensation
              comment: "Enable VT compensation\n"
              x-wbgen:
                type: BIT
                access_bus: READ_WRITE
                access_dev: READ_ONLY
                clock: clk_dac_i
          - field:
              name: cal_latch
              range: 1
              description: Latch calibration taps
              x-wbgen:
                type: MONOSTABLE
                clock: clk_dac_i
          - field:
              name: rst_in
              range: 2
              description: Reset line state
              x-wbgen:
                type: BIT
                access_bus: READ_ONLY
                access_dev: WRITE_ONLY
                clock: clk_dac_i
          - field:
              name: taps
              range: 11-3
              description: n Taps
              comment: "Value in number of taps\n"
              x-wbgen:
                type: SLV
                access_bus: READ_ONLY
                access_dev: WRITE_ONLY
                clock: clk_dac_i
    - reg:
        name: csr
        address: 0x00000008
        width: 32
        access: rw
        description: DAC Block Status/Control
        children:
          - field:
              name: test_en_i
              range: 0
              description: Enable test pattern on the I channel
              x-wbgen:
                type: BIT
                clock: clk_dac_i
          - field:
              name: test_en_q
              range: 1
              description: Enable test pattern on the Q channel
              x-wbgen:
                type: BIT
                clock: clk_dac_i