memory-map:
  name: acqCore
  description: Acquisition IP core memory map
  bus: axi4-lite-32
  size: 4k
  word-endian: little
  x-driver-edge:
    module-type: RF_ACQCORE
    generate-separate-library: true
  schema-version:
    core: 3.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-fesa: 2.0.0
    x-driver-edge: 3.0.0
    x-conversions: 1.0.0
    x-wbgen: 1.0.0
    x-map-info: 1.0.0
    x-enums: 1.0.0
  x-map-info:
    ident: 0x13
    memmap-version: 2.0.4
  x-hdl:
    name-suffix: _regs
    bus-attribute: Xilinx
    bus-granularity: byte
  x-enums:
    - enum:
        name: format
        width: 2
        children:
          - item:
              name: Signed
              value: 0
          - item:
              name: Unsigned
              value: 1
          - item:
              name: Float
              value: 2
    - enum:
        name: mode
        width: 3
        children:
          - item:
              name: noDecimation
              value: 0
          - item:
              name: peakDetection
              value: 1
          - item:
              name: horizontalDecimation
              value: 2
          - item:
              name: verticalDecimation
              value: 3
          - item:
              name: verticalAndHorizontalDecimation
              value: 4
          - item:
              name: booleanMode
              value: 5
  children:
    - submap:
        name: ipInfo
        address: 0x0
        filename: ipInfo.cheby
        include: true
    - reg:
        name: control
        description: Control
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: false
        children:
          - field:
              name: softReset
              description: IP core software reset
              range: 0
              x-hdl:
                type: autoclear
          - field:
              name: debugMode
              description: Enable the debug mode, acquisition of a known pattern
              range: 2
          - field:
              name: softStart
              description: software general start trigger / start all channels
              range: 3
              x-hdl:
                type: autoclear
          - field:
              name: softStop
              description: software general stop trigger / stop all channels
              range: 4
              x-hdl:
                type: autoclear
          - field:
              name: bufferAddrSel
              description: "selection of the memory base address: memory area 0 or 1"
              range: 5
          - field:
              name: circularBufferEnable
              description: circular buffer mode enable bit
              range: 6
          - field:
              name: acqArm
              description: acquisition arm
              range: 7
              comment: The IP must be arm before a start trigger is generated or received to start acquiring data.
              x-hdl:
                type: autoclear
    - reg:
        name: status
        description: status
        width: 32
        access: ro
        address: next
        x-fesa:
          persistence: false
          multiplexed: false
        children:
          - field:
              name: busy
              description: "external mode: buffer manager is busy, copying fifo content to external memory / internal mode: acquisition in progress on at least one buffer"
              range: 0
              preset: 0
          - field:
              name: armed
              description: acqusition is armed
              range: 2
              preset: 0
          - field:
              name: debugMode
              description: debug mode is implemented (1 = implemented, 0 = not implemented)
              range: 1
              preset: 0
    - reg:
        name: faults
        description: acqCore faults
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: false
          multiplexed: false
        x-hdl:
          write-strobe: true
          type: or-clr
        children:
          - field:
              name: buffManagerError
              description: buffer manager not responding, timeout reached
              range: 0
              x-fesa:
                alarm-level: ERROR
    - reg:
        name: bufferStartAddress0
        description: Start address of the buffer 0 where the acquisition data are stored
        width: 32
        type: unsigned
        access: ro
        address: next
        x-fesa:
          persistence: true
          unit: bytes
          multiplexed: false
        x-hdl:
          write-strobe: true
    - reg:
        name: bufferStartAddress1
        description: Start address of the buffer 1 where the acquisition data are stored
        width: 32
        type: unsigned
        access: ro
        address: next
        x-fesa:
          persistence: true
          unit: bytes
          multiplexed: false
        x-hdl:
          write-strobe: true
    - reg:
        name: bufferSize
        description: buffer size for each channel
        width: 32
        type: unsigned
        access: ro
        address: next
        x-fesa:
          persistence: true
          unit: samples [32bit word]
          multiplexed: false
    - reg:
        name: acqBufSel
        description: buffer selection and channel selection
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
        children:
          - field:
              name: bufferSelect
              description: "Selector of multiplexed channel:  0 to 31"
              range: 15-0
          - field:
              name: channelSelect
              description: "Selector of multiplexed channel:  0 to 11"
              range: 31-16
    - reg:
        name: acqControl
        description: Acquisition control
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
        x-hdl:
          write-strobe: true
          type: wire
          port: reg
        children:
          - field:
              name: enable
              description: Enable the channel or the buffer
              range: 0
          - field:
              name: softStartTrig
              description: Software manual start trigger
              range: 1
              x-hdl:
                type: autoclear
          - field:
              name: softStopTrig
              description: Software manual stop trigger
              range: 2
              x-hdl:
                type: autoclear
          - field:
              name: decimation
              description: select the decimation mode (only if acqDSP block is instantiated)
              range: 5-3
              x-enums:
                name: mode
          - field:
              name: frameSyncDisable
              range: 6
              description: Disable acquisition frame synchronization
          - field:
              name: startTrigMask
              range: 23-16
              description: start triggers mask
              comment: "22-16 = external triggers\n23 = software triggers\n"
          - field:
              name: stopTrigMask
              range: 31-24
              description: stop triggers mask
              comment: "30-24 = external triggers\n31 = software triggers"
    - reg:
        name: acqStatus
        description: Acquisition status
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: ro
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
        x-hdl:
          port: reg
        children:
          - field:
              name: enabled
              description: Buffer selected is enabled
              range: 31
          - field:
              name: available
              description: Channel selected is frozen and ready to be read out
              range: 30
          - field:
              name: complete
              description: Channel has wrapped around the end of the memory
              range: 29
          - field:
              name: acqDSP
              description: acqDSP block is implemented (1 = implemented, 0 = not implemented)
              range: 0
          - field:
              name: peakDetection
              description: Peak detection mode is implemented (1 = implemented, 0 = not implemented)
              range: 1
          - field:
              name: peakDetectionMode
              description: Describe the peak detection mode implemented (1 = Others signals, 0 = I/Q pair signal)
              range: 2
          - field:
              name: horizDecimation
              description: Horizontal decimation is implemented (1 = implemented, 0 = not implemented)
              range: 3
          - field:
              name: vertDecimation
              description: Vertical deciamtion is implemented (1 = implemented, 0 = not implemented)
              range: 4
          - field:
              name: booleanMode
              description: Boolean mode is implemented (1 = implemented, 0 = not implemented)
              range: 5
          - field:
              name: format
              description: Data format
              range: 7-6
              x-enums:
                name: format
          - field:
              name: busy
              description: The channel is acquiring data
              range: 28
          - field:
              name: empty
              description: FIFO empty status bit
              range: 27
          - field:
              name: fifoFullError
              range: 26
    - reg:
        name: acqTrigStatus
        description: Acquisition trigger status (1=triggered)
        comment: Multiplexed by acqChanSel register value, write to the register to reset the value
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
        x-hdl:
          write-strobe: true
          type: wire
          port: reg
        children:
          - field:
              name: startTrig
              description: start trigger status bits (1=triggered)
              range: 7-0
          - field:
              name: stopTrig
              description: stop trigger status bits (1=triggered)
              range: 15-8
    - reg:
        name: acqLength
        description: Acquisition length in samples
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          unit: samples
          multiplexed: true
        x-hdl:
          write-strobe: true
          type: wire
    - reg:
        name: acqFrzAddr
        description: Acquisition freeze address
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: ro
        address: next
        x-fesa:
          persistence: true
          unit: bytes
          multiplexed: true
    - reg:
        name: acqRateH1
        description: acquisition rate for horizontal decimation
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
        x-hdl:
          write-strobe: true
          type: wire
          port: reg
        children:
          - field:
              name: horizScaleMult
              description: multiplier for CIC filter output rescaling
              range: 15-0
              x-conversions:
                read: val/pow(2.0,15)
                write: val*pow(2.0,15)
              x-driver-edge:
                min-val: 1
                max-val: 32767
    - reg:
        name: acqRateH2
        description: acquisition rate for horizontal decimation
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
        x-hdl:
          write-strobe: true
          type: wire
          port: reg
        children:
          - field:
              name: horizScaleDiv
              description: divisor for CIC filter output rescaling
              range: 31-24
          - field:
              name: horizRate
              description: horizontal decimation rate
              range: 23-0
              x-driver-edge:
                min-val: 2
                max-val: 8388608
    - reg:
        name: acqRateV
        description: acquisition rate for vertical decimation
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
        x-driver-edge:
          max-val: 1024
          min-val: 2
        x-hdl:
          write-strobe: true
          type: wire
          port: reg
        children:
          - field:
              name: vertRate
              description: vertical decimation rate
              range: 10-0
              x-driver-edge:
                min-val: 2
                max-val: 1024
    - reg:
        name: acqThreshold
        description: acquisition peak detection trigger threshold
        comment: Multiplexed by acqChanSel register value
        width: 32
        access: rw
        address: next
        x-fesa:
          persistence: true
          multiplexed: true
          min-val: 0
        x-hdl:
          write-strobe: true
          type: wire
          port: reg
